import "./style.scss";
import Component, { aString } from "./component";

const app = document.querySelector<HTMLDivElement>("#app")!;

document.title = "Otp Generator";

app.innerHTML = `
  <header>
  <h1>This is me testing vanilla typescript</h1>
  <a href="https://vitejs.dev/guide/features.html" target="_blank">This is the Documentation for the project I'm doing</a>
  </header>
  <section>
  ${Component}
  </section>
  <main>
   your otp is <span>
   ${aString}
   </span>
  </main>
`;
